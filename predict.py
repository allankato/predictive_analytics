import pandas as pd
import numpy as np
from datetime import timedelta
from datetime import datetime

def predictor(startdate, enddate, physical):
    now=pd.to_datetime('today')
    startdate=pd.to_datetime(startdate)
    enddate=pd.to_datetime(enddate)
    projectdays=enddate-startdate
    projectdays=(projectdays/np.timedelta64(1,'D'))
    actualend_perf=0

    if(int(physical)>0):
        if(int(physical)<=100):
            covereddays=now-startdate
            covereddays=(covereddays/np.timedelta64(1, 'D'))
            newlifetime=(covereddays/int(physical))*100
            predicteddays=newlifetime-covereddays
            predicteddate=now+timedelta(days=predicteddays)
            #physical performance by end date
            actualend_perf=(projectdays/predicteddays)*100

        elif(int(physical)>100):
            predicteddate=enddate
    else:
        covereddays=now-startdate
        predicteddays=projectdays
        actualend_perf=100
        predicteddate="You have done nothing on the project and u still want to see SHIT?"

    return predicteddate, actualend_perf

if __name__=='__main__':
    alol=predictor('2017-06-30','2018-06-30',30)
    print(alol)
